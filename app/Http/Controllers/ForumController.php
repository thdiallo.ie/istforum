<?php

namespace App\Http\Controllers;

use App\Models\Forum;
use Illuminate\Http\Request;
use Alert;

class ForumController extends Controller
{
    public function index(){
        $forums = Forum::paginate(10);
        return view('forum.index', compact('forums'));
    }

    public function create(){
        $forum = new Forum();
        return view('forum.create', compact('forum'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'titre'=> 'required',
            'description'=> 'required|min:5',
            'date'=> 'required|date',
            'image'=> 'required|mines:jpg,png',
        ]);
        try {

            $image = $request->file('image');
            $nouveau_nom = uniqid().".".$image->getClientOriginalExtension();
            $image->storeAs('forum', $nouveau_nom, 'public');
            
            Forum::create([
                'titre' => $request->titre,
                'description' => $request->description,
                'date' => $request->date,
                'image' => $nouveau_nom,
            ]);

        }
        catch(\Exception $e)
        {
            dd($e);
        }

        Alert::success('Forum','Ajouté avec succès');
        return redirect()->route('forum.index');
    }

    public function edit(Forum $forum){
        return view('forum.edit', compact('forum'));
    }

    public function update(Request $request, Forum $forum){
        
        $this->validate($request, [
            'titre'=> 'required',
            'description'=> 'required|min:5',
            'date'=> 'required|date',
        ]);
        try {
            
            $forum->update([
                'titre' => $request->titre,
                'description' => $request->description,
                'date' => $request->date
            ]);
            
            if(isset($request->image))
            {
                $image = $request->file('image');
                $nouveau_nom = uniqid().".".$image->getClientOriginalExtension();
                $image->storeAs('forum', $nouveau_nom, 'public');
                $forum->update([
                    'image' => $nouveau_nom,
                ]);
            }

        }
        catch(\Exception $e)
        {
            dd($e);
        }

        Alert::success('Forum','Mise à jour avec succès');
        return redirect()->route('forum.index');
    }

    public function destroy(Forum $forum){
        $forum->delete();
        Alert::success('Forum','Suppression éffectué avec succès');
        return redirect()->route('forum.index');
    }
}
