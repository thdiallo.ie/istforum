<?php

namespace App\Http\Controllers;

use App\Models\Commentaire;
use App\Models\Forum;
use Illuminate\Http\Request;

class AccueilController extends Controller
{
    public function index(){
        $forums = Forum::limit(4)->latest()->get();
        return view('index', [
            'forums' => $forums
        ]);
    }

    public function detail(Forum $forum){
        return view('detail', compact('forum'));
    }
}
