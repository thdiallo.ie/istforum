<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    use HasFactory;

    protected $fillable = ['titre','description','image','date'];

    public function commentaires ()
    {
        return $this->hasMany(Commentaire::class);
    }
}
