@extends('layouts.app')

@section('content')
    <h1>Listes des forums</h1>
    <a href="{{ route('forum.create') }}" class="btn btn-primary">Ajouter un forum</a>
    <table class="table table-stripped table-bordered">
        <tr>
            <th>#</th>
            <th>Titre</th>
            <th>Description</th>
            <th>Image</th>
            <th>Date</th>
            <th></th>
        </tr>
        @foreach($forums as $key => $f)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$f->titre}}</td>
            <td>{{$f->description}}</td>
            <td> <img style="width:100px" src="{{ asset('storage/forum/'.$f->image) }}" alt=""> </td>
            <td>{{$f->date}}</td>
            <td>
                <a href="{{route('forum.edit', $f->id)}}" class="btn btn-warning text-white"> <i class="fa fa-pencil"></i> </a>
                <a href="#" onclick="document.getElementById('sup{{$f->id}}').submit()" class="btn btn-danger text-white"> <i class="fa fa-trash"></i> </a>

                <form id="sup{{$f->id}}" action="{{route('forum.destroy', $f->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                </form>
            </td>
        </tr>
        @endforeach
        
    </table>
    {{$forums->links()}}
@endsection