@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Nouveau Forum</h1>
    <form action="{{ route('forum.store') }}" method="POST" enctype="multipart/form-data" class="">
        @csrf
        
        @include('forum._form')
        <input type="submit" value="Enregistrer" class="btn btn-success">
    </form>
</div>
@endsection