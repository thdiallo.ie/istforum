@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Mise à jour du Forum</h1>
    <form action="{{ route('forum.update', $forum->id) }}" method="POST" enctype="multipart/form-data" class="">
        @csrf
        @method('PUT')
        @include('forum._form')
        <input type="submit" value="Mise à jour" class="btn btn-success">
    </form>
</div>
@endsection