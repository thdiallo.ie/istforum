<div class="form-group my-2">
    <label for="titre">Titre</label>
    <input type="text" name="titre" value="{{old('titre') ? old('titre') : $forum->titre}}" id="titre" class="form-control" >
    @error('titre') <span style="color:red">{{$message}}</span>@enderror
</div>
<div class="form-group my-2">
    <label for="description">Description</label>
    <textarea name="description" id="description" class="form-control"  cols="30" rows="3">{{old('description') ? old('description') : $forum->description}}</textarea>
    @error('description') <span style="color:red">{{$message}}</span>@enderror
</div>
@if($forum->id==0)
<div class="form-group my-2">
    <label for="image">Image</label>
    <input type="file" name="image" id="image" class="form-control" >
    @error('image') <span style="color:red">{{$message}}</span>@enderror
</div> 
@else
<div class="row">
    <div class="col-md-6">
        <div class="form-group my-2">
            <label for="image">Image</label>
            <input type="file" name="image" id="image" class="form-control" >
            @error('image') <span style="color:red">{{$message}}</span>@enderror
        </div> 
    </div>
    <div class="col-md-6">
        <img width="100%" src="{{asset('storage/forum/'.$forum->image)}}" alt="">
    </div>
</div>
@endif
<div class="form-group my-2">
    <label for="date">Date</label>
    <input type="date" name="date" value="{{old('date') ? old('date') : $forum->date}}" id="date" class="form-control" >
    @error('date') <span style="color:red">{{$message}}</span>@enderror
</div>