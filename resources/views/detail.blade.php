@extends('layouts.app')

@section('content')
<div class="container">
    <div style="display:flex;width:100%;height:350px;background:url('{{asset('storage/forum/'. $forum->image)}}');backgound-repeat:no-repeat;background-size:cover">
        
    </div>
    <h1>{{$forum->titre}}</h1>
    <p>
        {{$forum->description}}
    </p>
    <hr>
    <div>
        <div class="commentaire">
            <h2>Commentaires {{\App\Models\Commentaire::where('forum_id', $forum->id)->count()}}</h2>
           
            @foreach($forum->commentaires()->latest()->get() as $commentaire)

            <div class="comment {{ auth()->user()->id==$commentaire->user->id ? 'moi':''}}">
                <p>{{$commentaire->user->name}}</p>
                <p>{{$commentaire->commentaire}}</p>
            </div>
            @endforeach

        </div>
        <hr>
        <div>
            <form action="{{route('commentaire.store')}}" method="POST">
                @csrf 
                <input type="hidden" name="forum_id" value="{{$forum->id}}">
                <div class="form-control">
                    <label for="commentaire">Commentaire</label>
                    <textarea placeholder="Votre commentaire" name="commentaire" id="commentaire" class="form-control" cols="30" rows="10"></textarea>
                </div>

                <input type="submit" value="Envoyer" class="btn btn-success btn-block">
            </form>
        </div>
    </div>

</div>

@endsection


@section('css')
    <style>
        .comment {
            background:rgba(115, 115, 131, 0.263);
            width:80%;
        }

        .moi {
           margin-left:20%;
        }
    </style>
@endsection