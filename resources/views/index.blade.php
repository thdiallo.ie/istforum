<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IST</title>
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
</head>
<body>
    <header>
        <div class="logo">
            <img src="{{asset('images/Logo_IST_mamou.png') }}" alt="">
        </div>
        <nav>
            <ul>
                <li><a href="#">Accueil</a></li>
                <li><a href="#">Forum</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <a href="{{ route('login') }}">Connexion</a>
        </nav>
    </header>
    <div class="slider">

    </div>
    <main>
        <h1>Les dernières nouvelles</h1>

        <div class="news">
            @foreach($forums as $k => $forum)
            <a href="{{route('forum.detail', $forum->id)}}" class="new">
                <img src="{{asset('storage/forum/'.$forum->image) }}" alt="">
                <div>
                    <h5>{{$forum->titre}}</h5>
                    <span>  <i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($forum->date)->format('d M Y')}}</span>
                </div>
            </a>
            @endforeach
        </div>
    </main>

    <footer>
        &copy; 2023 IST-Mamou. Tous droits réservés.
        
    </footer>
    
</body>
</html>